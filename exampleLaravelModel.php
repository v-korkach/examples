<?php namespace App\Models\ExampleModel;

use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;

class StatisticHelper extends Loging {

	public static $timestamps 	= false;
	public static $table 		= '';

	public static $properties = array(
		'channels.id',
		'channels.impulse',
		'channels.name',
		'channels.power',
		'channels.unit',
		'channels.max AS maxvalue',
		'device_types.virtual_type AS virtual',
		'device_types.connection_interval as interval',
		'channel_types.analog',
		'channel_types.direction'
	);

	/**
	 * @param int $id_system
	 * @param array $ids_channels
	 * @return mixed
	 */
	public static function filterChannelsByIds($id_system,$ids_channels = array())
	{
		try
		{
			return static::prepareData()
				->where('gates.system_id','=',$id_system)
				->where_in('channels.id',$ids_channels)
				->get(static::$properties);
		}
		catch(Exception $e)
		{
			Log::write('MODEL_ERROR ' . __CLASS__ . ' args : ' . json_encode(func_get_args()),' ' . $e->getTraceAsString() . $e->getMessage() );
		}
	}

	/**
	 * @param int $id_system
	 * @return mixed
	 */
	public static function getAllChannels($id_system)
	{
		try
		{
			return static::prepareData()
				->where('gates.system_id','=',$id_system)
				->where(function($query)
				{
					$query->where('device_types.virtual_type','<>',4)
						  ->or_where_null('device_types.virtual_type');
				})
				->where(function($query)
				{
					$query->where('device_types.virtual_type','<>',5)
						  ->or_where('channel_types.number_in_device','<>',0)
						  ->or_where_null('device_types.virtual_type');
				})
				->get(static::$properties);
		}
		catch(Exception $e)
		{
			Log::write('MODEL_ERROR ' . __CLASS__ . ' args : ' . implode(' ',func_get_args()),' ' . $e->getTraceAsString() . $e->getMessage() );
		}
	}

   	/**
     * @param $id
     * @param $range
     * @param $limit
     */
    public static function getSectionData($id, $range, $limit, $cols)
    {
        $selectRow = '';
        $leftJoin = '';

        foreach($cols as $key => $value)
        {
            end($cols);
            if ($key !== key($cols))
            {
                $selectRow .= "`".$value."`.`value` as ".$value.", ";
            }
            else 
            {
                $selectRow .= "`".$value."`.`value` as ".$value." ";
            }
            $leftJoin .= "LEFT JOIN `sections_file_data` as ".$value." ON (sections_file.id=".$value.".section_file_id AND ".$value.".col_name = '".$value."' ) ";
        }


        $selectDatastring = "SELECT `sections_file`.* , `sections_file`.row_value as ".$mainColName->name.", ".$selectRow." FROM `sections_file` ".$leftJoin."WHERE sections_file.files_id = ".$id." ORDER BY `sections_file`.`id` ASC LIMIT ".$range." , ".$limit.";";

        $file_data = DB::select($selectDatastring);

        return $file_data;
    }


}