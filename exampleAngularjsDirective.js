'use strict';

/**
 * Author Vlad Korkach
 * Direcrive that Smoothscroll to element by anchor
 * @type {[type]}
 */
var app = angular.module('smoothScrollTo');

app.directive('anchorSmoothScroll', function($location) 
{  
    return 
    {
        restrict: 'A',
        replace: false,
        scope: 
        {
            'anchorSmoothScroll': '@'
        }, 
        link: function($scope, $element, $attrs) 
        {
            initialize();

            /**
             * initialize event listner
             */
            function initialize() 
            {
                createEventListeners();
            }

            /**
             * createEventListeners
             */
            function createEventListeners() 
            {
                $element.on('click', function() 
                {
                    $location.hash($scope.anchorSmoothScroll);
                    scrollTo($scope.anchorSmoothScroll);
                });
            }
 
            /**
             * scrollTo function
             * @param  element id
             */
            function scrollTo(eID) 
            {

                var i;
                var startY = currentYPosition();
                var stopY = elmYPosition(eID);
                var distance = stopY > startY ? stopY - startY : startY - stopY;
                if (distance < 100) 
                {
                    scrollTo(0, stopY); 
                    return;
                }
                var speed = Math.round(distance / 100);
                if (speed >= 20)
                {
                   speed = 20; 
                } 
                var step = Math.round(distance / 25);
                var leapY = stopY > startY ? startY + step : startY - step;
                var timer = 0;
                if (stopY > startY) 
                {
                    for (i = startY; i < stopY; i += step) 
                    {
                        setTimeout('window.scrollTo(0, '+leapY+')', timer * speed);
                        leapY += step; if (leapY > stopY) leapY = stopY; timer++;
                    } 
                    return;
                }
                for (i = startY; i > stopY; i -= step) 
                {
                    setTimeout('window.scrollTo(0, '+leapY+')', timer * speed);
                    leapY -= step; if (leapY < stopY) leapY = stopY; timer++;
                }
            }

            /**
             * currentYPosition
             * @return position
             */
            function currentYPosition() 
            {
                // Firefox, Chrome, Opera, Safari
                if (window.pageYOffset) 
                {
                    return window.pageYOffset;
                }
                // Internet Explorer 6 - standards mode
                if (document.documentElement && document.documentElement.scrollTop) 
                {
                    return document.documentElement.scrollTop;
                }
                // Internet Explorer 6, 7 and 8
                if (document.body.scrollTop) 
                {
                    return document.body.scrollTop;
                }
                return 0;
            }
 
            /**
             * elmYPosition
             * @param  element id
             * @return  coord
             */
            function elmYPosition(eID) 
            {
                var elm = document.getElementById(eID);
                var y = elm.offsetTop;
                var node = elm;
                while (node.offsetParent && node.offsetParent != document.body) 
                {
                    node = node.offsetParent;
                    y += node.offsetTop;
                } 
                return y;
            }
        }
    };
});
