'use strict';

/**
 * Author Vlad Korkach
 * controller that reset user pass by using api
 * @type {[type]}
 */
var app = angular.module('ResetPassCtrl');
app.controller('ResetPassCtrl', 
    [
        '$rootScope',
        '$scope',
        '$location',
        '$routeParams',
        '$window',
        'user',
        'api',
        'apiUrl',
        function ($rootScope, $scope, $location, $routeParams,$window ,user, api, apiUrl) 
        {
            $scope.resetPass = function()
            {
                var token = $routeParams.session;
                var pwd = $scope.pw1;
                var pwd2 = $scope.pw2;

                if($scope.pw1)
                {
                    api.resetConfirm(token,pwd, pwd2);
                    $('body').addClass('loading');
                }
            }

            $scope.reloadRoute = function() 
            {
                $window.location.reload();
            }


            $scope.$on('resetConfirm', function(event,res)
            {
                $('body').removeClass('loading');

                if(!res.error)
                {
                    
                    user.setUserData(res.user, res.token.access_token);

                    user.setAvatarImage(apiUrl+'uploads/user/'+res.user.image);
                    user.setCompanyImage(apiUrl+'uploads/company/'+res.user.company_image);

                    $scope.isLogin =  user.isLogin();

                    var userToken = user.getToken();
                    api.getEnabledOption(userToken);

                    $scope.userFullName =  user.getUserFullName();

                    var rightMenuLoading = angular.element('.right-menu');
                    rightMenuLoading.css('display', 'block');

                    window.location.href = '/';
                }
                else
                {
                    $scope.errorLoginMessage = res.messages;
                }
            })
        }
    ]
);

