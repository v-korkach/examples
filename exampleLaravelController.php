<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Validator;


class FileUtilsController extends Controller {

    /**
     *
     * Check and set file type after uploading
     * @param $data
     * @return bool|string
     */
    public static function _checkFileType($data)
    {
        switch($data)
        {
            case array_key_exists ('identification_code' ,  $data):
                return 'companies';
            case array_key_exists ('date' ,  $data):
                return 'history';
            case array_key_exists ('indicator_name' ,  $data):
                return 'economy';
            default:
                return 'sections';
        }
    }

    /**
     * Validate parse file to empty values end throw errors
     *
     * @param $savedFile - File data recently upload
     * @param $parse - File parse data.
     * @return array
     */
    public static function _validateParseFile($savedFile,$parse,$type)
    {
        switch($type)
        {
            case 'companies':
                return self::validateCompanyFile($savedFile,$parse);
            case 'history':
                return self::validateHistoryFile($savedFile,$parse);
            case 'sections':
                return self::validateSectionsFile($savedFile,$parse);
            case 'economy':
                return self::validateEconomyFile($savedFile,$parse);
            default:
                return false;
        }
    }

    /**
     * Validate company file data
     * @param $savedFile
     * @param $parse
     * @return array
     */
    public static function validateCompanyFile($savedFile,$parse)
    {
        $validationMessage = "File required field is empty or IBIC identifier is not unique.\n\n";
        $count = count($parse);
        $completed = 0;
        $error_flag = false;
        foreach($parse as $key => $value)
        {
            if(array_key_exists('ibic',$value) || array_key_exists('id',$value))
            {
                $responce = array(
                    'error'=>true, 
                    'criticalError' => true,
                    'message'=> "You are using illegal cols name like 'ibic' or 'id' in you csv file. Please change these cols names or delete them. The file data was not saved." 
                    );
                return $responce;
            }
            if(!array_key_exists('islamic_banker_identification_code_ibic',$value))
            {
                $responce = array(
                    'error'=>true,
                    'criticalError' => true, 
                    'message'=> "This file haven't required field identification_code - unique identifier for company.The file data was not saved"
                    );
                return $responce;
            }

            $validator = Validator::make(
                [
                    'ibic' =>  $value['identification_code'],
                    'file_id' => $savedFile->id,
                    'company_name' => $value['company_name'],
                    'address' => $value['address'],
                    'website' =>  $value['website'],
                    'country' => $value['country']
                ],
                [
                    'ibic' => 'required',
                    'file_id' => 'required',
                    'company_name' => 'required',
                    'address' => 'required',
                    'website' => 'required',
                    'country' => 'required',
                ]);

            if ($validator->fails())
            {
                $k = $completed+2;
                $validationMessage.= "Line: ".$k." - IBIC identifier: ".$value['islamic_banker_identification_code_ibic']."\n";
                $error_flag = true;
                continue;
            }
            ++$completed;
        }

        if($error_flag)
        {
            $responce = array(
                'error'=>true, 
                'message'=>$validationMessage, 
                'count' => $count, 
                'completed' => $completed
                );
        }
        else
        {
            $responce = array(
                'error'=>false, 
                'message'=>$validationMessage,
                 'count' => $count, 
                 'completed' => $completed
                 );
        }

        return $responce;
    }

    /**
     * Validation of history file
     *
     * @param $savedFile
     * @param $parse
     * @return array
     */
    public static function validateHistoryFile($savedFile,$parse)
    {
        $validationMessage = "File required field is empty or incorrect date format.\n\n";
        $count = count($parse);
        $completed = 2;
        $error_flag = false;
        foreach($parse as $key => $value)
        {
            if(!array_key_exists('date',$value))
            {
                $responce = array(
                    'error'=>true,
                    'criticalError' => true, 
                    'message'=> "This file haven't required field islamic_banker_identification_code_ibic - unique identifier for company.The file data was not saved"
                    );
                return $responce;
            }
            self::_dateValidationRules();
            $validator = Validator::make(
                [
                    'date' =>  $value['date'],
                    'file_id' => $savedFile->id,

                ],
                [
                    'date' => 'required|custom_date',
                    'file_id' => 'required',
                ]);

            if ($validator->fails())
            {
                $k = $completed;
                $validationMessage.= "Line: ".$k." - This cel must be required and date formatted\n";
                $error_flag = true;
                $completed+=1;
                continue;
            }
        }

        if($error_flag)
        {
            $responce = array(
                'error'=>true, 
                'message'=>$validationMessage, 
                'count' => $count, 
                'completed' => $completed
                );
        }
        else
        {
            $responce = array(
                'error'=>false, 
                'message'=>$validationMessage, 
                'count' => $count, 
                'completed' => $completed
                );
        }

        return $responce;
    }

    /**
     * Validate economy file
     *
     * @param $savedFile
     * @param $parse
     * @return array
     */
    public static function validateEconomyFile($savedFile,$parse)
    {
        $validationMessage = "File required fields is empty.\n\n";
        $count = count($parse);
        $completed = 0;
        $error_flag = false;

        foreach($parse as $key => $value)
        {
            $a = array(
                'country_name' =>  $value['country_name'],
                'country_code' => $value['country_code'],
                'indicator_name' => $value['indicator_name'],
                'indicator_code' =>  $value['indicator_code'],
                'file_id' => $savedFile->id
            );
            $b = array(
                'country_name' => 'required',
                'country_code' => 'required',
                'indicator_name' => 'required',
                'indicator_code' => 'required',
                'file_id' => 'required',
            );

            $validator = Validator::make($a, $b);

            if ($validator->fails())
            {
                $k = $completed+2;
                $error_flag = true;
                continue;
            }
            ++$completed;
        }

        if($error_flag)
        {
            $responce = array(
                'error'=>true, 
                'message'=>$validationMessage,
                'count' => $count, 
                'completed' => $completed
                );
        }
        else
        {
            $responce = array(
                'error'=>false, 
                'message'=>$validationMessage, 
                'count' => $count, 
                'completed' => $completed
                );
        }

        return $responce;
    }

    /**
     * Validate sections file
     *
     * @param $savedFile
     * @param $parse
     * @return array
     */
    public static function validateSectionsFile($savedFile,$parse)
    {
        reset($parse[0]);
        $first_key = key($parse[0]);
        $validationMessage = "File required field is empty or have duplicate values.\n\n";
        $count = count($parse);
        $completed = 0;
        $error_flag = false;
        $tmp = array();

        foreach($parse as $key => $value)
        {
            if(in_array($value[$first_key], $tmp))
            {
                $responce = array(
                    'error'=>true,
                    'criticalError' => true, 
                    'message'=> "You have duplicate values in main analytics cell of $first_key column. Please delete this raw and try again.\n"
                    );
                return $responce;
            }

            $tmp[] = $value[$first_key];

            $validator = Validator::make(
                [
                    'category' =>  $value[$first_key],
                ],
                [
                    'category' => 'required|string',
                ]);

            if ($validator->fails())
            {
                $k = $completed+2;

                $validationMessage.= "Line: ".$k." - This cel must be required and string formatted\n";
                $error_flag = true;
                continue;
            }

        }

        if($error_flag)
        {
            $responce = array(
                'error'=>true, 
                'message'=>$validationMessage,
                 'count' => $count, 
                 'completed' => $completed
                 );
        }
        else
        {
            $responce = array(
                'error'=>false, 
                'message'=>$validationMessage,
                'count' => $count, 
                'completed' => $completed
                );
        }

        return $responce;
    }


    public static function _checkColsNameTable($colName)
    {
        switch($colName)
        {
            case 'ibic':
                return 'companies';
            case 'company_name':
                return 'companies';
            case 'address':
                return 'companies';
            case 'website':
                return 'companies';
            case 'country':
                return 'companies';
            default:
                return 'companies_data';
        }

    }

    public static function _checkEditEmptyRequiredCols($cols)
    {
        switch($cols){
            case 'company_name':
                return false;
            case 'address':
                return false;
            case 'website':
                return false;
            case 'country':
                return false;
            default:
                return true;
        }
    }

    public static function _dateValidationRules()
    {
            Validator::extend('custom_date', function($attribute, $value, $parameters)
            {
                /**
                 * Description: Date in DD/MM/YYYY format. Fecha en formato DD/MM/AAAA.
                 * Matches: 28/12/2003 | 28/02/2003 | 29/02/2000
                 * Non-Matches: 28-02-2003 | 30/02/2003 | 28.02.2003
                 */
                $date1 = "(((0[1-9]|[12][0-9]|3[01])([/])(0[13578]|10|12)([/])(\d{4}))|(([0][1-9]|[12][0-9]|30)([/])(0[469]|11)([/])(\d{4}))|((0[1-9]|1[0-9]|2[0-8])([/])(02)([/])(\d{4}))|((29)(\.|-|\/)(02)([/])([02468][048]00))|((29)([/])(02)([/])([13579][26]00))|((29)([/])(02)([/])([0-9][0-9][0][48]))|((29)([/])(02)([/])([0-9][0-9][2468][048]))|((29)([/])(02)([/])([0-9][0-9][13579][26])))";
                /**
                 * Description: Mathces in format DD-MON-YYYY (hyphen between results). Validates for leap years. Ensures month is in uppercase.
                 * Matches: 9-MAY-1981 | 29-FEB-2004 | 25-DEC-1999
                 * Non-Matches: 09 MAY 1981 | 06 Jul 2003
                 */
                $date2 = "^((31(?! (FEB|APR|JUN|SEP|NOV)))|((30|29)(?! FEB))|(29(?= FEB (((1[6-9]|[2-9]\d)(0[48]|[2468][048]|[13579][26])|((16|[2468][048]|[3579][26])00)))))|(0?[1-9])|1\d|2[0-8])-(JAN|FEB|MAR|MAY|APR|JUL|JUN|AUG|OCT|SEP|NOV|DEC)-((1[6-9]|[2-9]\d)\d{2})$^";
                /**
                 * Description:
                 * Matches: 31-5-2004 , '03-05-2012'
                 *
                 */
                $date3 = "((0?[1-9]|[12][0-9]|3[01])-(0?[1-9]|1[0-2])-[12][0-9]{3})";
                /**
                 * Description: Mathces in format DD-MON-YYYY (hyphen between results). Validates for leap years. Ensures month is in uppercase.
                 * Matches: 9-May-1981 | 29-Feb-2004 | 25-Dec-1999
                 * Non-Matches: 09 MAY 1981 | 06 Jul 2003
                 */
                $date4 = "^(([0-9])|([0-2][0-9])|([3][0-1]))\-(Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec)\-\d{4}$^";
                /**
                 * '29-Feb-04'
                 */
                $date5 = "^(d{0}|(31(?!(Feb|feb|Apr|apr|Jun|jun|Sep|sep|Nov|nov)))|((30|29)(?!Feb|feb))|(‌​29(?=Feb|feb(((1[6-9]|[2-9]\d)(0[48]|[2468][048]|[13579][26])|((16|[2468][048]|[3‌​579][26])00)))))|(29(?=Feb|feb(((0[48]|[2468][048]|[13579][26])|((16|[2468][048]|‌​[3579][26])00)))))|(0?[1-9])|1\d|2[0-8])[- ](Jan|jan|feb|Feb|mar|Mar|may|May|apr|Apr|jul|Jul|Jun|jun|Aug|aug|Oct|oct|Sep|se‌​p|Nov|nov|dec|Dec)[- ]\d{2}$^";
                /**
                 * '2015-05-15'
                 */
                $date6 = "/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/";

                if(preg_match($date1 , $value))
                {
                    return $value = 'custom_date';
                }
                elseif (preg_match($date2 , $value))
                {
                    return $value = 'custom_date';
                }
                elseif (preg_match($date3 , $value))
                {
                    return $value = 'custom_date';
                }
                elseif (preg_match($date4 , $value))
                {
                        return $value = 'custom_date';
                }
                elseif (preg_match($date5 , $value))
                {
                        return $value = 'custom_date';
                }
                elseif (preg_match($date6 , $value))
                {
                        return $value = 'custom_date';
                }
                else
                {
                    return false;
                }
        });
    }

}