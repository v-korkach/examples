'use strict';

/**
 * Author Vlad Korkach
 * Service with api calls
 * @type {[type]}
 */
var app = angular.module('apiService');
app.service('api', 
    [
        '$http', 
        '$rootScope', 
        function($http, $rootScope)
        {
            return
            {
                /**
                 * Example call function
                 * @param  some data
                 * @return error or response from api
                 */
                exampleCall: function(data)
                {
                    if(data){
                        return $http(
                        {
                            method: 'POST',
                            url: '/someurl',
                            data: 
                            {
                                data: data
                            }
                        })
                        .success(function(response)
                        {
                            $rootScope.$broadcast('exampleCallResponce', response);
                        })
                        .error(function(err)
                        {
                            console.log(err);
                        });
                    }
                    else 
                    {
                        console.log('no data to change')
                    }
                }
            }
        }
    ]);


